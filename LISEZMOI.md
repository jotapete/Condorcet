Condorcet (thème for Omeka S)
=============================

> __Les nouvelles versions de ce modules et l’assistance pour Omeka S version 3.0
> et supérieur sont disponibles sur [GitLab], qui semble mieux respecter les
> utilisateurs et la vie privée que le précédent entrepôt.__

See [English readme].

[Condorcet] est le thème pour [Omeka S] conçu pour le site [bibnum.campus-condorcet.fr].

**ATTENTION**: Les logos ne peuvent pas être réutilisés sans permission exprès.


Installation
------------

Consulter la documentation utilisateur pour [installer un module].

Décompresser les fichiers et renommer le dossier du thème comme vous le voulez,
de préférence sans espace et sans caractères spéciaux.

Le thème fonctionne mieux avec les modules associés :
- Module [Advanced Search] pour gérer la recherche.
- Module [Block Plus] pour gérer les pages de site.
- Module [Iiif Server] pour gérer la visionneuse.
- [Mirador], [Universal Viewer], et [Image Server] pour la page de contenu.
- Module [Reference] pour afficher les facettes dans les résultats avec
  l’adaptateur interne. Il n’est pas nécessaire pour les moteurs de recherche
  externe qui peuvent gérer les facettes nativement. Utiliser au moins la
  version 3.4.16 et la dernière si possible.

Le thème actuel n’est pas conçu pour créer des expositions ou des contenus
statiques. Si vous en avez besoin, il convient de mettre à jour le thème et les
blocs manquants.


Attention
---------

Utilisez-le à vos propres risques.

Il est toujours recommandé de sauvegarder vos fichiers et vos bases de données
et de vérifier vos archives régulièrement afin de pouvoir les reconstituer si
nécessaire.


Dépannage
---------

Voir les problèmes en ligne sur la page des [questions du thème] du GitLab.


Licence
-------

### Thème (sans logos)

Ce thème, **excepté les logos**, est publié sous la licence [CeCILL v2.1],
compatible avec [GNU/GPL] et approuvée par la [FSF] et l’[OSI].

Ce logiciel est régi par la licence CeCILL de droit français et respecte les
règles de distribution des logiciels libres. Vous pouvez utiliser, modifier
et/ou redistribuer le logiciel selon les termes de la licence CeCILL telle que
diffusée par le CEA, le CNRS et l’INRIA à l’URL suivante "http://www.cecill.info".

En contrepartie de l’accès au code source et des droits de copie, de
modification et de redistribution accordée par la licence, les utilisateurs ne
bénéficient que d’une garantie limitée et l’auteur du logiciel, le détenteur des
droits patrimoniaux, et les concédants successifs n’ont qu’une responsabilité
limitée.

À cet égard, l’attention de l’utilisateur est attirée sur les risques liés au
chargement, à l’utilisation, à la modification et/ou au développement ou à la
reproduction du logiciel par l’utilisateur compte tenu de son statut spécifique
de logiciel libre, qui peut signifier qu’il est compliqué à manipuler, et qui
signifie donc aussi qu’il est réservé aux développeurs et aux professionnels
expérimentés ayant des connaissances informatiques approfondies. Les
utilisateurs sont donc encouragés à charger et à tester l’adéquation du logiciel
à leurs besoins dans des conditions permettant d’assurer la sécurité de leurs
systèmes et/ou de leurs données et, plus généralement, à l’utiliser et à
l’exploiter dans les mêmes conditions en matière de sécurité.

Le fait que vous lisez actuellement ce document signifie que vous avez pris
connaissance de la licence CeCILL et que vous en acceptez les termes.

### Actifs numériques

Les logos ne sont pas libérés et restent sous copyright.

### Bibliothèques

Consulter le dossier asset/vendor pour les copyrighs et les autres licences.


Copyright
---------

* Copyright Cathy Drévillon, Véronica Holguin, Denis Chiron, Daniel Berthereau pour [Sempiternelia], 2021-2022


[Condorcet]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Condorcet
[English readme]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Condorcet
[bibnum.campus-condorcet.fr]: https://bibnum.campus-condorcet.fr
[Omeka S]: https://omeka.org/s
[installer un module]: http://dev.omeka.org/docs/s/user-manual/modules/#installing-modules
[Advanced Search]: https://gitlab.com/Daniel-KM/Omeka-S-module-AdvancedSearch
[Block Plus]: https://gitlab.com/Daniel-KM/Omeka-S-module-BlockPlus
[Iiif Server]: https://gitlab.com/Daniel-KM/Omeka-S-module-IiifServer
[Mirador]: https://gitlab.com/Daniel-KM/Omeka-S-module-Mirador
[Universal Viewer]: https://gitlab.com/Daniel-KM/Omeka-S-module-UniversalViewer
[Image Server]: https://gitlab.com/Daniel-KM/Omeka-S-module-ImageServer
[Reference]: https://gitlab.com/Daniel-KM/Omeka-S-module-Reference
[questions du thème]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Condorcet/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[GitLab]: https://gitlab.com/Daniel-KM
[Sempiternelia]: https://sempiternelia.net
