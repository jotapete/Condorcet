$(function () {

    const body = $('body');
    const mainElement = $('main');

    // Menus
    const headerMenu = $('.header-menu');
    const headerSubMenu = $('.header-submenu');
    const headerSearchMenu = $('.header-search-menu');

    // Buttons
    const burgerButton = $('.c-hamburger'); // Hamburger
    const headerSearchBtn = $('.header-search-button'); // Loupe

    // Actions
    const openSearchMenuBtn = $('a.open-search-menu');
    const openHeaderMenuBtn = $('a.open-header-menu');
    const openHeaderSubMenuBtn = $('a.open-header-submenu');

    // Formulaires
    var chosenOptions = {
        allow_single_deselect: true,
        disable_search_threshold: 10,
        width: '100%',
        include_group_label_in_selected: true,
    };

    const closeMenu = function() {
        headerMenu.removeClass('opened');
        burgerButton.removeClass('active');
        openHeaderMenuBtn.closest('li').removeClass('active');
        headerSubMenu.removeClass('opened');
    };

    const closeSearchMenu = function() {
        headerSearchMenu.removeClass('opened');
        headerSearchBtn.closest('li').removeClass('active');
    };

    openHeaderMenuBtn.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        closeSearchMenu();

        // Open Menu
       headerMenu.toggleClass('opened');
       burgerButton.toggleClass('active');
       openHeaderMenuBtn.closest('li').toggleClass('active');
    });

    openHeaderSubMenuBtn.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('li').toggleClass('opened');
    });

    openSearchMenuBtn.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        closeMenu();

        // Open Search Menu
       headerSearchMenu.toggleClass('opened');
       $(this).closest('li').toggleClass('active');
    });

    mainElement.on('click', function(e) {
        closeMenu();
        closeSearchMenu();
    });

    $('.toggle').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.toggle-parent').toggleClass('opened');
    });

    /* Collections : pagination */

    $('.browse-contents-see-all').on('click', function(e) {
        // Pas sur les blocs.
        if ($(this).closest('.blocks').length) {
            return;
        }
        e.preventDefault();
        $(this).hide();
        $('.browse-itemsets-pagination').show();
    });

    /* Collection : full description */

    $('.itemset-show-description-more').on('click', function(e) {
        e.preventDefault();
        $('.itemset-show-header-full-description').slideToggle();
    });

    $('.itemset-show-description-close').on('click', function(e) {
        e.preventDefault();
        $('.itemset-show-header-full-description').slideToggle();
    });

    /* Items : onglets */
    $('.item-show-metadata-tabs a').on('click', function(e) {
        e.preventDefault();

        $('.item-show-metadata-tabs a').closest('li').removeClass('active');
        $(this).closest('li').addClass('active');

        const tabContent = $(this).closest('.item-show-metadata-parent').find('.item-show-metadata-tab-contents > li');
        tabContent.removeClass('active');
        tabContent.eq($(this).closest('li').index()).addClass('active');
    });

    $('.item-show-metadata-mobile-tab a').on('click', function(e) {
        e.preventDefault();

        const tabContent = $(this).closest('.item-show-metadata-parent').find('.item-show-metadata-tab-contents > li');
        tabContent.removeClass('active');

        const currentTab = $(this).closest('.item-show-metadata-tab-contents > li');
        currentTab.addClass('active');

        const tabs = $(this).closest('.item-show-metadata-parent').find('.item-show-metadata-tabs > li');
        tabs.removeClass('active');
        tabs.eq(currentTab.index()).addClass('active');
    });

    // Correction carte leaflet (https://github.com/Leaflet/Leaflet/issues/2738). TODO Remplacer display:none par visibility:hidden.
    $('.item-show-metadata-tabs a.tab-mapping').on('click', function(e) {
        $('#mapping-section').trigger('o:section-opened');
    });

    $('.item-show-metadata-languages a').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.item-show-metadata-languages').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        const lang = $(this).closest('li').data('lang');
        $(this).closest('.item-show-metadata-tab-content').find('.item-show-metadata').removeClass('current-language');
        $(this).closest('.item-show-metadata-tab-content').find('.item-show-metadata[data-lang=' + lang + ']').addClass('current-language');
    });

    $('.copy-clipboard').on('click', function() {
        const text = $(this).data('citation');
        if (!text) {
            return false;
        }
        navigator.clipboard.writeText(text);
        alert(text + ' copié dans le presse-papier !');
        return false;
    });

    /* Search */

    // Prise en compte du champ spécifique : adaptation de la requête soumise.
    $('.search-box-content form').on('submit', function(e) {
        const form = $(this);
        const q = form.find('[name=q]').val();
        const by = form.find('[name=by]').val();
        if (by && by.length) {
            form
                .append('<input type="hidden" name="filter[0][join]" value="and"/>')
                .append('<input type="hidden" name="filter[0][field]" value="'
                    + (by === 'period' ? 'dcterms_date_s' : (by === 'subject' ? 'dcterms_subject_ss' : ''))
                    + '"/>')
                .append('<input type="hidden" name="filter[0][type]" value="eq"/>')
                .append('<input type="hidden" name="filter[0][value]" value="" id="form_value"/>')
                .find('#form_value')
                .val(q);
            // form.find('[name=q]').val('');
            // form.find('[name=by]').val('');
        }
        return true;
    });

    const closePopups = function() {
        body.removeClass('popup-opened advanced-search-popup-opened search-by-map-popup-opened search-infos-popup-opened');
    };
    $(document).on('click', 'body.popup-opened', function(e) {
      if (!$(e.target).closest('.is-popup-content').length) {
            closePopups();
        }
    });
    $(document).on('keyup', 'body.popup-opened', function(e) {
        if (e.key === 'Escape') {
            closePopups();
        }
    });

    /* Search by map popup */

    $('.search-by-map-button a').on('click', function(e) {
        e.stopPropagation();
        body.addClass('popup-opened search-by-map-popup-opened');
        // Correction carte leaflet (https://github.com/Leaflet/Leaflet/issues/2738). TODO Remplacer display:none par visibility:hidden.
        $('#mapping-section').trigger('o:section-opened');
    });

    $('.search-by-map-close a').on('click', function(e) {
        e.stopPropagation();
        body.removeClass('popup-opened search-by-map-popup-opened');
    });

    $('.home-search-infos a').on('click', function(e) {
        e.stopPropagation();
        body.addClass('popup-opened search-infos-popup-opened');
    });

    /* Advanced Search popup */

    $('.advanced-search-button a').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        body.addClass('popup-opened advanced-search-popup-opened');
    });

    $('.search-infos-popup-header .search-popup-close a').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        body.removeClass('popup-opened search-infos-popup-opened');
    });

    $('.advanced-search-close a').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        body.removeClass('popup-opened advanced-search-popup-opened');
    });

    /**
     * Manage the multi-fieldset for the filters of the advanced search.
     *
     * @todo Reuse the omeka js for advanced search?
     *
     * Note: the id "#advanced-search" is duplicated inside the sidebar and in the search page,
     * and may be slightly different for item set pages.
     */
    var resetFieldsetFilter = function (fieldsetFilter) {
        let filterJoin = $(fieldsetFilter.find('> select:nth-of-type(1)'));
        let filterField = $(fieldsetFilter.find('> select:nth-of-type(2)'));
        let filterType = $(fieldsetFilter.find('> select:nth-of-type(3)'));
        let filterValue = $(fieldsetFilter.find('input[type=text]')); // name = [value]
        if (filterJoin.length) {
            filterJoin.val(filterJoin.find('option:nth-child(1)').val()).select();
            filterJoin.trigger('chosen:updated');
        }
        if (filterField.length) {
            filterField.val(filterField.find('option:nth-child(1)').val()).select();
            filterField.trigger('chosen:updated');
        }
        if (filterType.length) {
            filterType.val(filterType.find('option:nth-child(1)').val()).select();
            filterType.trigger('chosen:updated');
        }
        filterValue.val('');
    }
    var prepareAdvancedSearch = function (fieldsetSearchFilters) {
        fieldsetSearchFilters = $(fieldsetSearchFilters);
        var fieldsetFilters = fieldsetSearchFilters.find('.search-filter');
        var addMore = fieldsetSearchFilters.closest('form').find('.advanced-search-more a');
        // Display only the first and filled filters.
        $(fieldsetSearchFilters.find('.search-filter:not(:first)')).each(function(index) {
            // Manage standard input and display of "has/has not a value".
            let filterType = $(this).find('select[name="filter[' + (index + 1) + '][type]"]');
            let filterValue = $(this).find('input[type=text]'); // name = [value]
            if (filterValue.val() === ''
                && (filterType.val() === 'undefined' || (filterType.val() !== 'ex' && filterType.val() !== 'nex'))
            ) {
                resetFieldsetFilter($(this));
                $(this).hide();
            } else {
                $(this).show();
            }
        });
        // Display the remove filter and add-more button only if needed.
        if (fieldsetFilters.length > 1) {
            // By default, the advanced search is hidden, so use display, not :visible.
            let displayedFilters = 0;
            fieldsetFilters.each(function() {
                displayedFilters += $(this).css('display') === 'none' ? 0 : 1;
            });
            if (displayedFilters < fieldsetFilters.length) {
                addMore.show().prop('disabled', false);
            } else {
                addMore.hide().prop('disabled', true);
            }
        } else {
            addMore.hide().prop('disabled', true);
        }
    }
    prepareAdvancedSearch($('.advanced-search-popup .search-filters'));

    // Move and display the first hidden fieldset when add-more is clicked.
    $('.advanced-search-more a:not([disabled])').on('click', function (e) {
        e.preventDefault();
        let fieldsetSearchFilters = $(this).closest('form').find('.search-filters');
        $(fieldsetSearchFilters.find('.search-filter:hidden')[0]).show();
        var addMore = fieldsetSearchFilters.closest('form').find('.advanced-search-more a');
        if (fieldsetSearchFilters.find('.search-filter:visible').length < fieldsetSearchFilters.find('.search-filter').length) {
            addMore.show().prop('disabled', false);
        } else {
            addMore.hide().prop('disabled', true);
        }
    });
    // Manage the close button for a search filter.
    $('.advanced-search-popup .search-filter').on('click', '.advanced-search-remove-filter', function (e) {
        e.preventDefault();
        e.stopPropagation();
        let fieldsetFilter = $(this).closest('.search-filter');
        fieldsetFilter.hide();
        fieldsetFilter.closest('form').find('.advanced-search-more a').show().prop('disabled', false);
        resetFieldsetFilter(fieldsetFilter);
    });

    /* Search results */

    $('a.search-results-open-filters').on('click', function(e) {
        e.preventDefault();
        const toggler = $(this);
        const filters = $('.search-results-filters .toggle-parent');
        if (toggler.data('status') === 'closed') {
            toggler.data('status', 'opened').text(toggler.data('label-opened'));
            filters.removeClass('opened').addClass('opened');
        } else {
            $(this).data('status', 'closed').text(toggler.data('label-closed'));
            filters.removeClass('opened');
        }
    });

    $('a.search-results-toggle-filter').on('click', function(e) {
        e.preventDefault();
        if ($(this).css('pointer-events') !== 'none') {
            $(this).closest('.toggle-parent').toggleClass('opened');
        }
    });

    // When all facets are opened or closed one by one, update the main toggler.
    $('.search-results-filters .toggle').on('click', function(e) {
        e.preventDefault();
        const toggler = $('.search-results-open-filters');
        const facets = $('.search-results-filters .toggle-parent');
        if (facets.filter('.opened').length === facets.length) {
            toggler.data('status', 'opened').text(toggler.data('label-opened'));
        } else if (facets.not('.opened').length === facets.length) {
            toggler.data('status', 'closed').text(toggler.data('label-closed'));
        }
    });

    $('.search-facets-selected a').on('click', function(e) {
        // Reload with the link when there is no button to apply facets.
        if (!$('.apply-facets').length) {
            return true;
        }
        e.preventDefault();
        $(this).closest('li').hide();
        var facetName = $(this).data('facetName');
        var facetValue = $(this).data('facetValue');
        $('.search-facet-item input').each(function() {
            if ($(this).prop('name') === facetName && $(this).prop('value') === facetValue) {
                $(this).prop('checked', false);
            }
        });
    });

    $('.search-facets').on('change', 'input[type=checkbox]', function(e) {
        e.preventDefault();
        if (!$('.apply-facets').length) {
            window.location.href = $(this).data('url');
        }
    });

    /* Sort selector links (depending if server of client build) */
    $('.search-sort select').on('change', function(e) {
        // Sort fields don't look like a url.
        e.preventDefault();
        var sort = $(this).val();
        if (sort.substring(0, 6) === 'https:' || sort.substring(0, 5) === 'http:') {
            window.location = sort;
        } else if (sort.substring(0, 1) === '/') {
            window.location = window.location.origin + sort;
        } else {
            var searchParams = new URLSearchParams(window.location.search);
            searchParams.set('sort', $(this).val());
            window.location.search = searchParams.toString();
        }
    });

    /* Resize */

    const userBar = $("#user-bar");
    const headerBar = $('body > header');

    function updateMinHeight() {

        const minHeight = $(window).height() - $('header').outerHeight(true) - $('footer').outerHeight(true);
        mainElement.css('min-height', minHeight + 'px');

        if (headerBar.css('position') === 'fixed') {
            const userBarHeight = userBar.length ? userBar.outerHeight() : 0;
            mainElement.css('padding-top', (userBarHeight + headerBar.outerHeight()) + 'px');
            headerBar.css('margin-top', userBarHeight + 'px');
        } else {
            headerBar.css('margin-top', '0px');
            mainElement.css('padding-top', '0px');
        }

    }
    $(window).on('resize', updateMinHeight);
    updateMinHeight();

    /* Formulaires */

    // Omeka admin.js.
    $('.chosen-select').chosen(chosenOptions);
    // Along with CSS, this fixes a known bug where a Chosen dropdown at the
    // bottom of the page breaks layout.
    // @see https://github.com/harvesthq/chosen/issues/155#issuecomment-173238083
    $(document).on('chosen:showing_dropdown', '.chosen-select', function(e) {
        var chosenContainer = $(e.target).next('.chosen-container');
        var dropdown = chosenContainer.find('.chosen-drop');
        var dropdownTop = dropdown.offset().top - $(window).scrollTop();
        var dropdownHeight = dropdown.height();
        var viewportHeight = $(window).height();
        if (dropdownTop + dropdownHeight > viewportHeight) {
            chosenContainer.addClass('chosen-drop-up');
        }
    });
    $(document).on('chosen:hiding_dropdown', '.chosen-select', function(e) {
        $(e.target).next('.chosen-container').removeClass('chosen-drop-up');
    });

});
